class breakoutGame {
    canvas = null
    ctx = null

    bricks = null
    ball = null
    paddle = null

    paddleController = null
    ballController = null
    collisionController = null

    interval = null

    constructor(canvas) {
        this.canvas = canvas
        this.ctx = this._getCtxFromCanvas()

        // Spawn paddle on the center of canvas
        this.paddleX = (this.canvas.width - PaddleSettings.paddleWidth) / 2

        // Spawn ball on the center of canvas and near the bottom of canvas
        let x = this.canvas.width / 2
        let y = this.canvas.height - 30

        this.bricks = new BrickController(this.canvas, this.ctx)
        this.ball = new Ball(this.ctx, new BallMotionHandler(2, -2), x, y, BallSettings.ballRadius)
        this.paddle = new Paddle(
            this.ctx,
            this.paddleX,
            this.canvas.height - PaddleSettings.paddleHeight,
            PaddleSettings.paddleHeight,
            PaddleSettings.paddleWidth)

        this.paddleController = new PaddleController(new KeysHandler())
        this.ballController = new BallController()
        this.collisionController = new CollisionController()

        this.interval = setInterval(this.iteration, GameSettings.delay)
    }

    _getCtxFromCanvas = () => {
        return canvas.getContext('2d');
    }
    
    _isWin = () => {
        if(!this.bricks.isEmptyBrickList()) {
            alert('You won')

            document.location.reload()
            clearInterval(this.interval)
        }
    }

    gameOver = () => {
        alert('Game over')

        document.location.reload()
        clearInterval(this.interval)
    }

    iteration = () => {
        this._draw()
        this._collision()
        this._motion()
    }

    _draw = () => {
        this.ctx.clearRect(0, 0, canvas.width, canvas.height)

        this.bricks.draw()
        this.ball.draw()
        this.paddle.draw()
    }

    _collision = () => {
        this.collisionController.checkBallToPaddle(this.ball, this.paddle, this.gameOver)
        this.collisionController.checkBallToBricks(this.ball, this.bricks.getItemsList(), this.bricks.dropBrick)
        this.collisionController.checkBallToCanvasWalls(this.ball)
        
        this._isWin()
    }

    _motion = () => {
        this.ballController.moveBall(this.ball)
        this.paddleController.movePaddle(this.paddle)
    }
}