class BrickController {
    canvas
    ctx

    count
    layers

    countInLayer = 0
    itemsList = []

    constructor(canvas, ctx, count, layers) {
        this.canvas = canvas
        this.ctx = ctx

        if (count) {
            this.count = count
        } else {
            this.count = LevelSettings.countItems
        }

        if (layers) {
            this.layers = layers
        } else {
            this.layers = LevelSettings.layers
        }

        this._generateItemsList()
        this._checkLeftItems()
    }

    _generateItemsList = () => {
        this.countInLayer = Math.floor(this.count / this.layers)

        const positionRow = this._calcWidths(this.countInLayer, BrickSettings.width)

        for (let i = 0; i < this.layers; i++) {
            let brickRow = []

            for (let j = 0; j < this.countInLayer; j++) {
                let brick = new Brick(
                    this.ctx,
                    positionRow[j],
                    BrickSettings.topMargin * (i + 1) + BrickSettings.height * i,
                    BrickSettings.height,
                    BrickSettings.width,
                    BrickSettings.brickColor
                )

                brick.setId({
                    x: i,
                    y: j
                })

                brickRow.push(brick)
            }

            this.itemsList.push(brickRow)
        }
    }

    _checkLeftItems = () => {
        const leftItems = this.count - (this.layers * this.countInLayer);

        if (leftItems > 0) {
            const positionRow = this._calcWidths(leftItems, BrickSettings.width)

            let brickRow = []
            for (let j = 0; j < leftItems; j++) {
                let brick = new Brick(
                    this.ctx,
                    positionRow[j],
                    BrickSettings.topMargin * (this.layers + 1) + BrickSettings.height * this.layers,
                    BrickSettings.height,
                    BrickSettings.width,
                    BrickSettings.brickColor
                )

                brick.setId({
                    x: this.layers,
                    y: j
                })

                brickRow.push(brick)
            }

            this.itemsList.push(brickRow)
        }
    }

    _calcWidths = (count, width) => {
        if (count === 1) {
            return [this.canvas.width / 2 - width / 2]
        }

        if (count > 1) {
            const sumOfWidths = count * width
            const spaceSize = (this.canvas.width - sumOfWidths - BrickSettings.horizontalOffset * 2) / (count - 1)

            let list = []
            for (let i = 0; i < count; i++) {
                list.push((width + spaceSize) * i + BrickSettings.horizontalOffset)
            }

            return list
        }
    }

    getItemsList = () => {
        return this.itemsList
    }

    isEmptyBrickList = () => {
        let count = 0

        this.itemsList.forEach((brickRow) => {
            brickRow.forEach(() => {
                count++;
            })
        })

        return count
    }

    dropBrick = (id) => {
        delete this.itemsList[id.x][id.y]
    }

    draw = () => {
        this.itemsList.forEach((bricksList) => {
            bricksList.forEach((Brick) => {
                Brick.draw()
            })
        })
    }
}