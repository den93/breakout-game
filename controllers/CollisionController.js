class CollisionController {
    checkBallToBricks = (Ball, bricks, event) => {
        bricks.forEach((arrayOfBricks) => {
            arrayOfBricks.forEach((Brick) => {
                if (this._calcRangeFromBallToBrick(Ball, Brick)) {
                    this._assignBallAndBrickActions(Ball, Brick, event)
                }
            })
        })
    }

    _assignBallAndBrickActions = (Ball, Brick, event) => {
        let action = this._checkBallToBrick(Ball, Brick)

        switch (action) {
            case 'top':
            case 'bottom':
                Ball.motion.changeVerticalDirection()
                event(Brick.getId())
                break;
            case 'left':
            case 'right':
                Ball.motion.changeHorizontalDirection()
                event(Brick.getId())
                break;
            default:
                break;
        }
    }

    /**
     * Need to be rechecked a lot
     * @param Ball
     * @param Brick
     * @returns {string}
     * @private
     */
    _checkBallToBrick = (Ball, Brick) => {
        let direction

        if (Ball.x + Ball.radius <= Brick.x &&
            Ball.y + Ball.radius >= Brick.y + Brick.height &&
            Ball.y + Ball.radius <= Brick.y
        ) {
            direction = 'left'
        } else if (
            Ball.y >= Brick.y + Brick.height &&
            Ball.x >= Brick.x &&
            Ball.x <= Brick.x + Brick.width
        ) {
            direction = 'bottom'
        } else if (
            Ball.y <= Brick.y + Brick.height &&
            Ball.x >= Brick.x &&
            Ball.x <= Brick.x + Brick.width
        ) {
            direction = 'top'
        } else {
            direction = 'right'
        }

        return direction;
    }

    /**
     * If ball close to brick - we can start calculate walls collision
     * For calculation we are using Ball radius and Half of diagonal of Brick
     * like the biggest distance between Brick and Ball for starting checking
     * @param Ball
     * @param Brick
     * @returns {boolean}
     * @private
     */
    _calcRangeFromBallToBrick = (Ball, Brick) => {
        let brickCenter = Brick.getCenterOfBrick()
        let distance = Math.sqrt(
            Math.pow(Math.abs(brickCenter.x - Ball.x), 2) + Math.pow(Math.abs(brickCenter.y - Ball.y), 2)
        )

        return distance < Ball.radius + Brick.getHalfDiagonal()
    }


    checkBallToCanvasWalls = (Ball) => {
        if (Ball.y + Ball.motion.getVerticalMotion() < Ball.radius) {
            Ball.motion.changeVerticalDirection()
        }

        if (
            Ball.x + Ball.motion.getHorizontalMotion() < Ball.radius ||
            Ball.x + Ball.motion.getHorizontalMotion() > canvas.width - Ball.radius
        ) {
            Ball.motion.changeHorizontalDirection()
        }
    }

    checkBallToPaddle = (Ball, Paddle, gameOver) => {
        if (Ball.y + Ball.motion.getVerticalMotion() > canvas.height - Ball.radius) {
            if (Ball.x > Paddle.x && Ball.x < Paddle.x + Paddle.paddleWidth) {
                Ball.motion.changeVerticalDirection()
            } else {
                gameOver()
            }
        }
    }
}