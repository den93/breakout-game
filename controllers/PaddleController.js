class PaddleController {
    keysHandler = null

    constructor(keysHandler) {
        this.keysHandler = keysHandler
    }

    movePaddle = (Paddle) => {
        if (this.keysHandler.rightPressed) {
            Paddle.x += PaddleSettings.paddleSpeedMove

            if (Paddle.x + PaddleSettings.paddleWidth > canvas.width) {
                Paddle.x = canvas.width - PaddleSettings.paddleWidth
            }
        } else if (this.keysHandler.leftPressed) {
            Paddle.x -= PaddleSettings.paddleSpeedMove

            if (Paddle.x < 0) {
                Paddle.x = 0
            }
        }
    }
}