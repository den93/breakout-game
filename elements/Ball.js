class Ball extends MovableElement {
    x = 0;
    y = 0
    radius = 0
    color = BallSettings.ballColor

    constructor(ctx, motion, x, y, radius, color) {
        super(ctx, motion);

        this.x = x
        this.y = y
        this.radius = radius

        if (color) {
            this.color = color
        }

        return this;
    }

    draw = () => {
        this.ctx.beginPath();
        this.ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
        this.ctx.fillStyle = this.color
        this.ctx.fill()
        this.ctx.closePath()
    }
}