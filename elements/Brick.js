class Brick extends Element {
    x
    y
    height
    width
    color = "Black"

    halfDiagonal = 0
    centerOfBrick = {
        x: 0,
        y: 0
    }

    id = {
        x: 0,
        y: 0
    }

    constructor(ctx, x, y, height, width, color) {
        super(ctx);

        this.x = x
        this.y = y
        this.height = height
        this.width = width
        this.color = color

        this.halfDiagonal = Math.sqrt(Math.pow(height, 2) + Math.pow(width, 2)) / 2
        this.centerOfBrick = this._calcCenterOfBrick()
    }

    draw = () => {
        this.ctx.beginPath()
        this.ctx.rect(this.x, this.y, this.width, this.height)
        this.ctx.fillStyle = this.color
        this.ctx.fill()
        this.ctx.closePath()
    }

    setId(id) {
        this.id = id
    }

    getId() {
        return this.id
    }

    getHalfDiagonal = () => {
        return this.halfDiagonal
    }

    getCenterOfBrick = () => {
        return this.centerOfBrick
    }

    _calcCenterOfBrick = () => {
        return {
            x: this.x + this.width / 2,
            y: this.y + this.height / 2
        }
    }
}