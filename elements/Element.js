/**
 * Main parent for element of the canvas
 */
class Element {
    ctx = null

    constructor(ctx) {
        this.ctx = ctx
    }
}