class MovableElement extends Element {
    /**
     * Settings for next move
     * @type {MotionHandler}
     */
    motion = null

    constructor(ctx, motion) {
        super(ctx);

        this.motion = motion
    }
}