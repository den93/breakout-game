class Paddle extends Element {
    x = 0;
    y = 0
    paddleHeight = 0
    paddleWidth = 0
    color = PaddleSettings.paddleColor

    constructor(ctx, x, y, paddleHeight, paddleWidth, color) {
        super(ctx);

        this.x = x
        this.y = y
        this.paddleHeight = paddleHeight
        this.paddleWidth = paddleWidth

        if(color) {
            this.color = color
        }

        return this;
    }

    draw = () => {
        this.ctx.beginPath()
        this.ctx.rect(this.x, this.y, this.paddleWidth, this.paddleHeight)
        this.ctx.fillStyle = this.color
        this.ctx.fill()
        this.ctx.closePath()
    }
}