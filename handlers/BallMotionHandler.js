class BallMotionHandler extends MotionHandler{
    constructor(x, y) {
        super(x, y);
    }

    changeVerticalDirection() {
        this._y = -this._y
    }

    changeHorizontalDirection() {
        this._x = -this._x
    }
}