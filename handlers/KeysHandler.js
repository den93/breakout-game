class KeysHandler {
    rightPressed = false
    leftPressed = false

    constructor() {
        this.buttonsHandler()
    }

    buttonsHandler = () => {
        document.addEventListener('keyup', this._keyUpHandler, false)
        document.addEventListener('keydown', this._keyDownHandler, false)
    }

    _keyDownHandler = (e) => {
        if (e.key == "Right" || e.key == "ArrowRight") {
            this.rightPressed = true
        } else if (e.key == "Left" || e.key == "ArrowLeft") {
            this.leftPressed = true
        }
    }

    _keyUpHandler = (e) => {
        if (e.key == "Right" || e.key == "ArrowRight") {
            this.rightPressed = false
        } else if (e.key == "Left" || e.key == "ArrowLeft") {
            this.leftPressed = false
        }
    }
}