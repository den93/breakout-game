/**
 * Handler for counting of elements motion
 * F.e. Balls, yes Balls
 */
class MotionHandler {
    _x = 0
    _y = 0

    constructor(x, y) {
        this._x = x
        this._y = y
    }

    getHorizontalMotion() {
        return this._x
    }

    getVerticalMotion() {
        return this._y
    }

}