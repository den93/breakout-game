class BallSettings {
    /**
     * Default ball radius
     * @type {number}
     */
    static ballRadius = 10

    /**
     * Mail color of the ball
     * @type {string}
     */
    static ballColor = '#BB99FF'
}