class BrickSettings {
    /**
     * Width of bricks
     * @type {number}
     */
    static width = 50
    /**
     * Height of bricks
     * @type {number}
     */
    static height = 20

    /**
     * Offset from left and right side from screen
     * @type {number}
     */
    static horizontalOffset = 30
    /**
     * Margin between layers of bricks
     * @type {number}
     */
    static topMargin = 10

    /**
     * Color of bricks
     * @type {string}
     */
    static brickColor = '#0095DD'
}