class GameSettings {
    /**
     * Main delay for whole game
     * @type {number}
     */
    static delay = 10

    /**
     * Main application color
     * @type {string}
     */
    static appColor = '#0095DD'
}