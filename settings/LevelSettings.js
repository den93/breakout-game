class LevelSettings {
    /**
     * Count of bricks on lvl
     * @type {number}
     */
    static countItems = 36

    /**
     * Count of default generated layers
     * @type {number}
     */
    static layers = 5
}