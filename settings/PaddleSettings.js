class PaddleSettings {
    /**
     * Speed of paddle (pixels per iteration by one delay)
     * @type {number}
     */
    static paddleSpeedMove = 7

    /**
     * The height of Paddle
     * @type {number}
     */
    static paddleHeight = 10

    /**
     * The width of paddle
     * @type {number}
     */
    static paddleWidth = 80

    /**
     * The main color of the paddle
     * @type {string}
     */
    static paddleColor = '#338833'
}